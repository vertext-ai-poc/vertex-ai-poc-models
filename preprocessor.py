#we recommend users not to add any changes in this files
#Project : Vertex-AI POC
#Company : Avaus Marketing Innovations
#Authors : Betelhem Desta ,Karolina Meyer,Paweł Drabczyk

from google.cloud import bigquery
import helper

class Preprocessor:
  

    def __init__(self, projectName, datasetName, trainingTable):

        self.projectName = projectName
        self.datasetName = datasetName
        self.trainingTable = trainingTable

    
    #The function used to transform raw input table into a table with additional data_split_column (posiible values 'TEST', 'VALIDATE' and 'TRAIN') called 'splitcol'
    #the destination table is then used to train BigQuery autoML models and to create other tables for the needs of training classical BigQuery ML models
    #the share of test and validate samples can be specified in global parameters
    #The function returns job object to monitor the status of the job
    def createProcessingTable(self, processingTablePath, trainingTablePath):

        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)

        trainThreshold = 1 - helper.configDict['evalFraction'] - helper.configDict['testFraction']
        evalThreshold = 1 - helper.configDict['testFraction']

        QUERY = """CREATE TABLE IF NOT EXISTS
                      `{CREATE_TABLE_NAME}`
                    OPTIONS( expiration_timestamp=TIMESTAMP_ADD
                    (CURRENT_TIMESTAMP(), INTERVAL 1 DAY)) 
                    AS
                    WITH
                        CTE AS (
                            SELECT
                                *,
                                RAND() AS r
                            FROM
                                `{RAW_TABLE}`
                        )
                        SELECT
                            * EXCEPT(r),
                            CASE
                                WHEN r<{TRAIN_THRESHOLD} THEN 'TRAIN'
                                WHEN r BETWEEN {TRAIN_THRESHOLD} AND {EVAL_THRESHOLD} THEN 'VALIDATE'
                                WHEN r>{EVAL_THRESHOLD} THEN 'TEST'
                            END
                            AS splitcol,
                            CASE
                                WHEN r BETWEEN {TRAIN_THRESHOLD} AND {EVAL_THRESHOLD} THEN TRUE
                                WHEN r<{TRAIN_THRESHOLD} THEN FALSE
                                WHEN r>{EVAL_THRESHOLD} THEN TRUE
                            END  
                            AS splitcolbq
                        FROM
                            CTE""".format(CREATE_TABLE_NAME=processingTablePath, RAW_TABLE=trainingTablePath, TRAIN_THRESHOLD=trainThreshold, EVAL_THRESHOLD=evalThreshold)
        queryJob = client.query(QUERY, job_config=jobConfig)  # API request
        return queryJob

    
    def preprocessing(self,trainingTable):

        trainingTablePath= self.projectName+ "." +self.datasetName + "." + trainingTable
        processingTablePath = trainingTablePath + "_temp"
        processingTablePath
                
        print("Preparing {PROCESSING_TABLE}.".format(PROCESSING_TABLE=processingTablePath))
        createProcessingTableJob = self.createProcessingTable(processingTablePath, trainingTablePath) 
        helper.jobStatus([createProcessingTableJob], 10)
        print("Training table is ready.")
