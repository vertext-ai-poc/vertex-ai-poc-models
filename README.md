# Vertext AI Proof of Concept
The more detailed documentation can be found in google document https://docs.google.com/document/d/1osxz75zw1LCVP3Sh5FGHEyh06TOQHATEwYJedL73_Dg/edit?usp=sharing

Google released a stand alone ML framework for data scientist, ML ops and data engineers - Vertex AI. In this project a proof of concept was developed by 
Avaus Bootcampers July, 2021 edition.

This repository contains the following files:

1.	**README.md** here you find the description on how to develop a model, test and predict using the vertext-ai-poc.pynb and basic. 
2.	**vertext-ai-poc.pynb** this notebook is created using jupyter instance in Vertext AI. You will also find this file under notebooks in Vertext AI console. 
3.	**configuration file** here you can find step-by-step instruction how to create Vertex AI instance. 
			
Steps to develop model using vertext-ai-poc.pynb and additional important information:

1. Before you upload your dataset make sure there are no column name that use BigQuery keywords. Example: ALL, AND, ANY, ARRAY, AS, BETWEEN, BY, CASE, CAST, COLLATE, CUBE, CURRENT, DEFAULT, DEFINE, DESC, DISTINCT. A complete list can be found [here](https://cloud.google.com/bigquery/docs/reference/standard-sql/lexical#reserved_keywords).
2. Make sure you have dataset created on bigquery using EU-region
3. The notebook is dividided for 3 parts: defining parameters by user, function definitions - automated, 
4. In the first part of the notebook you have to define dataset and model details: GCP project name, dataset, table, column names, target column and model name
5. In the first part of the notebook you can tune ML model hyperparameters. Leave the default values if not needed.
6. If you have data with already splitted tables (for training, evaluating and testing) you have to define data split column in the first part of the notebook and DO NOT CALL createAutoMLTable, createBQTrainEvalTable and createBQTestTable table preparation functions.
7. The path of the additional tables (located elsewhere then specified dataset) should have following format 'project.dataset.tablename'
8. In the second part of the notebook there are data preprocessing and model function definitions. DO NOT MAKE ANY CHANGES HERE and run all these cells.
9. In the third part of the notebook you call 'one click' functions:
	* data preprocessing
	* training
	* evaluating
	* predicting
10. Model versioning is provided - after running oneClickF1ScoreEvaluate function only the best model version from training call will be stored in BigQuery (vol1_x, vol2_y, etc).
11. You can see the train, validate and test split tables in BigQuery dataset.
12. Model prediction are stored in BigQuery as a table.
13. Make sure the number of iteration and buget hours are not very large values, this will raise the cost of training on GCP.
14. Cleaning up, such as deleting unused models and tables is the responsibility of the user.
   

There are comments under each cell section navigate if there are any.
			



