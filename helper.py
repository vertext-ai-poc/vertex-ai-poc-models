#we recommend users not to add any changes in this files
#Project : Vertex-AI POC
#Company : Avaus Marketing Innovations
#Authors : Betelhem Desta ,Karolina Meyer,Paweł Drabczyk

import time
import yaml
from google.cloud import bigquery



with open(r'config.yaml') as file:
    configDict = yaml.load(file, Loader=yaml.FullLoader)

        
#Monitor bigquery jobs
#The function to check the status of all jobs passed in a list, if one of them is still RUNNING the function waits
#for its finish and continues checking. SleepTime is the period between checks 
def jobStatus(jobList, sleepTime):
        client = bigquery.Client()

        for job in jobList:
            while 1:
                jobCheck = client.get_job(job.job_id)
                if jobCheck.state!='DONE':
                    time.sleep(sleepTime)
                else:
                    if jobCheck.errors:
                        print("Job {JOB_ID} is currently in state {JOB_STATE} with ERRORS: {JOB_CHECK}".format(JOB_ID=jobCheck.job_id, JOB_STATE=jobCheck.state,
                                                                                                            JOB_CHECK=jobCheck.errors))
                    break
                    
#Function to cancel jobs, it requires list of jobs as a parameter
def cancelJob(jobList,sleepTime):
        client = bigquery.Client()
        
        for job in jobList:
            job_cancel = client.cancel_job(job.job_id, location=job.location)
            
        for job in jobList:
            while 1:
                jobCheck = client.get_job(job.job_id, location=job.location)
                if jobCheck.state!='DONE':
                    time.sleep(sleepTime)
                else:
                    print("Job {JOB_ID} has been cancelled. State {JOB_STATE}".format(JOB_ID=jobCheck.job_id, JOB_STATE=jobCheck.state))
                    break