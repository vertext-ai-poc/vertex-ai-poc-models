#we recommend users not to add any changes in this files
#Project : Vertex-AI POC
#Company : Avaus Marketing Innovations
#Authors : Betelhem Desta ,Karolina Meyer,Paweł Drabczyk


from google.cloud import bigquery
import time


class Train():
    
    
    #updates of the value of this hyperparameters is handled in congig.yaml

    def __init__(self, projectName,datasetName,trainingTable,
                      modelName,labelColumns,
                      classWeights, columnNames,jobNumber,
                      maxIterations, nTreesList,maxTreeDepthList,
                      subsetValue):
        self.projectName = projectName
        self.datasetName = datasetName
        self.trainingTable = trainingTable
        self.modelName = modelName
        self.labelColumns = labelColumns
        self.classWeights = classWeights
        self.columnNames= columnNames
        self.jobNumber = jobNumber
        self.maxIterations = maxIterations
        self.nTreesList =nTreesList 
        self.maxTreeDepthList = maxTreeDepthList
        self.datasetPath = self.projectName+ "." +self.datasetName
        self.trainingTablePath= self.projectName+ "." +self.datasetName + "." + self.trainingTable
        self.modelNamePath =self.datasetPath +"."+self.modelName
        self.subsetValue = subsetValue
        
     
    #training model using classical BigQuery model, 
    #because of using batch queries the jobs are run in parallel
    #the function finishes just after submiting the job and training goes on in BigQuery
    #the job is returned from the function to allow user to check its status

    def training(self, modelNamePath,modelType, maxIterations, nTrees, maxTreeDepth):

        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
        
        BQTrainEvalTablePath =self.trainingTablePath+"_temp"
 
       
        QUERY = """ CREATE OR REPLACE MODEL `{MODEL_NAME}` OPTIONS ( model_type='{MODEL_TYPE}', NUM_PARALLEL_TREE = {N_TREES}, 
                        MAX_TREE_DEPTH = {MAX_TREE_D} , MAX_ITERATIONS = {MAX_ITER}, labels = ['{LABEL_COLUMN}'], AUTO_CLASS_WEIGHTS = {CLASS_WEIGHTS},
                        DATA_SPLIT_METHOD = 'CUSTOM', DATA_SPLIT_COL = 'splitcolbq') AS
                    SELECT
                      {COLUMN_NAMES}
                    FROM
                      `{TABLE_NAME}`
                    WHERE
                      splitcol='TRAIN' OR splitcol='VALIDATE'
                    """.format(MODEL_NAME=modelNamePath, MODEL_TYPE=modelType, \
                                        N_TREES=nTrees, MAX_TREE_D=maxTreeDepth, MAX_ITER=maxIterations,\
                                        LABEL_COLUMN=self.labelColumns,
                                        CLASS_WEIGHTS=self.classWeights,
                                        COLUMN_NAMES=self.columnNames,
                                        TABLE_NAME=BQTrainEvalTablePath,
                                        SUBSET_VALUE=self.subsetValue)

    
        queryJob = client.query(QUERY, job_config=jobConfig)  # API request   
       
        return queryJob
    

    def returnLatestVersions(self):
    
        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
      
     
      
        modelNamePathParts = self.modelNamePath.split(".")
        project = modelNamePathParts[0]
        dataset = modelNamePathParts[1]
        model = modelNamePathParts[2]
        
        Query =  """SELECT table_id, size_bytes 
                    FROM `{PROJECT}.{DATASET}.__TABLES__` 
                    WHERE REGEXP_CONTAINS(table_id, '{MODEL_NAME}_vol([0-9]{{1,4}})_*') 
                    ORDER BY table_id DESC""".format(PROJECT=project, DATASET=dataset, MODEL_NAME=model)
        
        
        job = client.query(Query, job_config=jobConfig)  # API request
        rowIterator = job.result()
        totalRows = rowIterator.total_rows

        if totalRows == 0:
            return 0
        else:
            versionsList = []
            for row in rowIterator:
                latestVersionFullName = row.table_id
                latestVersionWithModelType = latestVersionFullName.split("_vol")[1]
                latestVersion = latestVersionWithModelType.split("_")[0]
                versionsList.append(int(latestVersion))
            return max(versionsList)

    
    #this function combines and orchestrates training models based on user selection
    #selectModel is used in config.yaml to pass user selction on training autoMl,Classic or both model
    def oneClickTraining(self):
   
        latestVersion = self.returnLatestVersions()
        self.currentVersion = latestVersion + 1
       
        trainJobsList = []
        trainedmodelNamePathsList = []
        
        def modelTrainingBigquery():
        
          
            #create logistic regresion model
            trainJobsList.append(self.training(self.modelNamePath +str('_vol') + str(self.currentVersion) + '_' + str(self.jobNumber), 
            'LOGISTIC_REG', self.maxIterations,10, 10))
            trainedmodelNamePathsList.append(self.modelNamePath+str('_vol')+str(self.currentVersion) + '_' + str(self.jobNumber))
            self.jobNumber = self.jobNumber + 1

            #creating gradient boosting trees
            for maxTreeDepth in self.maxTreeDepthList:
                for nTrees in  self.nTreesList :
                    trainJobsList.append(self.training(self.modelNamePath +str('_vol')+str(self.currentVersion) + '_' + str(self.jobNumber),
                    'BOOSTED_TREE_CLASSIFIER', self.maxIterations,nTrees, maxTreeDepth))
                    trainedmodelNamePathsList.append(self.modelNamePath+str('_vol')+str(self.currentVersion) + '_' + str(self.jobNumber))
                    self.jobNumber = self.jobNumber + 1
                    
            return trainJobsList, trainedmodelNamePathsList
            
        print("i am model path in OneClick:",self.modelNamePath)
        return modelTrainingBigquery()
