#we recommend users not to add any changes in this files
#Project : Vertex-AI POC
#Company : Avaus Marketing Innovations
#Authors : Betelhem Desta ,Karolina Meyer,Paweł Drabczyk

import time
from google.cloud import bigquery
from helper import jobStatus


class Evaluator():

    def __init__(self,projectName, datasetName, keepAllTrainedModels):
        
        self.projectName=projectName 
        self.datasetName = datasetName
        self.keepAllTrainedModels = keepAllTrainedModels
        self.allModelsList = []
        

    def evaluate(self, metric, modelList, testData, subsetValue, sleepTime=5):

        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
        jobList = []

        for model in modelList:
            if testData != None:
                Query =  """SELECT {METRIC} AS metricResult 
                            FROM 
                            ML.EVALUATE(MODEL `{MODEL_NAME}`,  (SELECT * FROM `{TEST_TABLE}` WHERE splitcol='{SUBSET_VALUE}'))""".format(METRIC=metric, MODEL_NAME=model, TEST_TABLE=testData, SUBSET_VALUE=subsetValue)
            else:
                Query =  """SELECT {METRIC} AS metricResult 
                            FROM 
                            ML.EVALUATE(MODEL `{MODEL_NAME}`)""".format(METRIC=metric, MODEL_NAME=model)
            jobList.append(client.query(Query, job_config=jobConfig))  # API request

        jobStatus(jobList, sleepTime)
        resultList = []
        for job in jobList:
            temp = next(job.result()).metricResult
            print(temp)
            resultList.append(temp)

        maximum = max(resultList)
        maximumIndex = resultList.index(maximum)    
        [bestModelName, bestMaximumMetric] = [modelList[maximumIndex], maximum]

        if self.keepAllTrainedModels == 1:
            return [bestModelName, bestMaximumMetric]
        else:
            for bqmodel in modelList:                      #loop through the list of bqmodels 
                if bqmodel !=  bestModelName:
                    client.delete_model(bqmodel)            # Make an API request 
                    print("Deleted model '{BQ_MODEL}'.".format(BQ_MODEL=bqmodel))
            return [bestModelName, bestMaximumMetric]
    
    def F1Evaluate(self, modelList, testData, subsetValue, sleepTime=5):

        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
        jobList = []

        for model in modelList:
            if testData != None:
                Query =  """WITH CTE AS 
                            (
                              SELECT
                                threshold,
                                true_positives/(true_positives+0.5*(false_positives+false_negatives)) AS f1_score
                              FROM
                                ML.ROC_CURVE (MODEL `{MODEL}`,
                                             (
                                               SELECT
                                                 *
                                               FROM
                                                 `{TESTDATA}`
                                               WHERE 
                                               splitcol='{SUBSET_VALUE}'),
                                               GENERATE_ARRAY(0.01,1.,0.01)))
                             SELECT
                               threshold,
                               f1_score
                             FROM
                               CTE
                             WHERE
                               f1_score=(SELECT MAX(f1_score) FROM CTE)""".format(MODEL=model, TESTDATA=testData, SUBSET_VALUE=subsetValue)
            else:
                Query =  """WITH CTE AS 
                            (
                              SELECT
                                threshold,
                                true_positives/(true_positives+0.5*(false_positives+false_negatives)) AS f1_score
                              FROM
                                ML.ROC_CURVE (MODEL `{MODEL}`)
                            )
                             SELECT
                               threshold,
                               f1_score
                             FROM
                               CTE
                             WHERE
                               f1_score=(SELECT MAX(f1_score) FROM CTE)""".format(MODEL=model)
            jobList.append(client.query(Query, job_config=jobConfig))  # API request

        jobStatus(jobList, sleepTime)

        resultList = []
        resultThresholdList = []
        for job in jobList:
            temp = next(job.result())
            resultList.append(temp.f1_score)
            resultThresholdList.append(temp.threshold)

        maximum = max(resultList)
        maximumIndex = resultList.index(maximum)
        [bestModelName, bestMaximumMetric, bestThreshold] = [modelList[maximumIndex], maximum, resultThresholdList[maximumIndex]]

        if self.keepAllTrainedModels == 1:
            return [bestModelName, bestMaximumMetric, bestThreshold, resultThresholdList]
        else:
            for bqmodel in modelList:                      #loop through the list of bqmodels 
                if bqmodel !=  bestModelName:
                    client.delete_model(bqmodel)  # Make an API request 
                    print("Deleted model '{BQ_MODEL}'.".format(BQ_MODEL=bqmodel))
            return [bestModelName, bestMaximumMetric, bestThreshold, resultThresholdList]
    
    
    def returnModelList(self, modelName):
        
        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)

        project = self.projectName
        dataset = self.datasetName
        model = modelName

        Query =  """SELECT table_id, size_bytes 
                    FROM `{PROJECT}.{DATASET}.__TABLES__` 
                    WHERE REGEXP_CONTAINS(table_id, '^{MODEL_NAME}_vol([0-9]{{1,4}})_*') 
                    ORDER BY table_id DESC""".format(PROJECT=project, DATASET=dataset, MODEL_NAME=model)


        job = client.query(Query, job_config=jobConfig)  # API request
        rowIterator = job.result()
        totalRows = rowIterator.total_rows

        if totalRows == 0:
            self.allModelsList = []
            return []
        else:
            allModelsList = []
            for row in rowIterator:
                allModelsList.append(project + "."+ dataset + "." + row.table_id)
            self.allModelsList = allModelsList
            return allModelsList
    