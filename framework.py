#we recommend users not to add any changes in this files
#Project : Vertex-AI POC
#Company : Avaus Marketing Innovations
#Authors : Betelhem Desta ,Karolina Meyer,Paweł Drabczyk


from google.cloud import bigquery
from preprocessor import Preprocessor
from trainer      import Train
from evaluator    import Evaluator
import helper 



#Framework class is used to combine and orchestrate all the class
#Helper class contains the reading from yamal file and other helper functions 

class Framework():
    
   
    def __init__(self,
                      projectName = helper.configDict["projectName"],
                      datasetName = helper.configDict["frameworkDatasetName"],
                      keepAllTrainedModels=helper.configDict['keepAllTrainedModels'],
                      classWeights = helper.configDict['classWeights'],                
                      maxIterations= helper.configDict['maxIterations'],
                      nTreesList = helper.configDict['nTreesList'],
                      maxTreeDepthList =helper.configDict['maxTreeDepthList'],
                    ):
        
        self.projectName =projectName
        self.datasetName = datasetName
        self.keepAllTrainedModels= keepAllTrainedModels
        self.classWeights = classWeights
        self.maxIterations = maxIterations
        self.nTreesList = nTreesList
        self.maxTreeDepthList = maxTreeDepthList
        self.jobNumber = 1
        self.trainedModelList = []
        self.allModelsList = []
        self.bestModel = ' '
        self.bestThreshold = 0.5
        self.thresholdDict = {} 
  


    # data preprocessing using preprocessor.py   
#     def preprocessing(self,trainingTable): 

#         self.preprocessor = Preprocessor(self.projectName,self.datasetName,trainingTable)

#         return self.preprocessor.preprocessing(trainingTable) #Save the tables created after data split to BigQuery ML

    #training models using train.py
    def train(self,trainingTable,modelName, labelColumns, columnNames, metric='roc_auc', subsetValue='TRAIN'):

        
        self.preprocessor = Preprocessor(self.projectName, self.datasetName, trainingTable)
        self.preprocessor.preprocessing(trainingTable) #Save the tables created after data split to BigQuery ML
        
        if columnNames[0] == "*":
            partsCapital = columnNames.split('EXCEPT(')
            partsSmall = columnNames.split('except(')
            if len(partsCapital) == 1 and len(partsSmall) == 1:
                self.columnNames = columnNames + ' EXCEPT(splitcol)'
            elif len(partsCapital) != 1:
                self.columnNames = partsCapital[0] + ' EXCEPT(splitcol, ' + partsCapital[1]
            elif len(partsSmall) != 1:
                self.columnNames = partsSmall[0] + ' except(splitcol, ' + partsSmall[1]
        else:
            self.columnNames = columnNames + ", splitcolbq, " + labelColumns
        
            
        self.trainer = Train(self.projectName,self.datasetName,trainingTable, 
                      modelName,labelColumns, self.classWeights,
                      self.columnNames,self.jobNumber,self.maxIterations,
                      self.nTreesList,self.maxTreeDepthList, subsetValue)
        
        [trainJobsList, trainedmodelNamePathsList] = self.trainer.oneClickTraining()
        
        print("Training started...")
        helper.jobStatus(trainJobsList, 1)

        print("Training finished. Evaluation of the trained models on validate sample started...")
        [bestModel, bestModelValidateMetric, bestModelValidateThreshold] = self.evaluate(metric=metric, 
        evaluationTable=trainingTable, modelName=trainedmodelNamePathsList, subsetValue='VALIDATE')

        print("Evaluation of the the trained models finished. Evaluating the best performing model on test sample started...")
        [bestModel, bestModelTestMetric, bestModelThreshold] = self.evaluate(metric=metric, 
        evaluationTable=trainingTable, modelName=bestModel, subsetValue='TEST')

        print("Best performing model={BEST_MODEL} with {METRIC}={BEST_MODEL_TEST_METRIC}".format(BEST_MODEL=bestModel,
        METRIC=metric, BEST_MODEL_TEST_METRIC=bestModelTestMetric))
        self.trainedModelList = trainedmodelNamePathsList

        return [trainJobsList,trainedmodelNamePathsList]

    #Evalute models using evaluator.py
    def evaluate(self, metric='f1_score', evaluationTable=None, modelName=None, subsetValue='VALIDATE'):
        
        self.evaluator = Evaluator(self.projectName,self.datasetName,self.keepAllTrainedModels)
        if evaluationTable != None:
            temp = evaluationTable.split('.')
            if len(temp) == 1:
                evaluationTable = self.projectName + "." + self.datasetName + "." + evaluationTable + "_temp"
            else:
                evaluationTable = evaluationTable + "_temp"
            
       
        if modelName == None:
            if self.allModelsList != []:
                modelName = self.allModelsList
            elif self.trainedModelList != []:
                modelName = self.trainedModelList
            else:
                print("No model list provided or stored in an framework object!")

        if type(modelName) != list:
            temp = modelName.split('.')
            if len(temp) == 1:
                modelName = self.projectName + "." + self.datasetName + "." + modelName
            modelNameList = [modelName]
        else:
            modelNameList = []
            for model in modelName:
                temp = model.split('.')
                if len(temp) == 1:
                    modelNameList.append(self.projectName + "." + self.datasetName + "." + model)
                else:
                    modelNameList.append(model)
                    
        if metric == 'f1_score':
            [bestModel, bestModelMaximumMetric, bestModelThreshold, resultThresholdList] = self.evaluator.F1Evaluate(modelNameList, evaluationTable, subsetValue)
            for i in range(len(modelNameList)):
                self.thresholdDict[modelNameList[i]] = resultThresholdList[i]
        elif metric == 'roc_auc' or  metric == 'log_loss':
            [bestModel, bestModelMaximumMetric] = self.evaluator.evaluate(metric, modelNameList, evaluationTable, subsetValue)
            for i in range(len(modelNameList)):
                self.thresholdDict[modelNameList[i]] = 0.5
            bestModelThreshold = 0.5
        
        self.bestModel = bestModel
        self.bestThreshold = bestModelThreshold
        return [bestModel, bestModelMaximumMetric, bestModelThreshold]

    
    def predict(self,predictionTable,modelName, labelColumn, columnNames):

        client = bigquery.Client()
        jobConfig = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
        path = self.projectName + "." + self.datasetName
        prediction_label = "predicted_" + labelColumn
    
        def predictAssit(predictionTablePath,modelNamePath,BQTestTablePath):
                
                threshold=0.5

                QUERY = """CREATE OR REPLACE TABLE `{TABLE_NAME}` AS 
                                        SELECT * EXCEPT ({PRED_LABEL}) 
                                        FROM ML.PREDICT(MODEL `{MODEL_NAME}`,
                                            (SELECT {COL_NAMES}
                                            FROM `{TEST_TABLE}`),
                                            STRUCT({THRESHOLD} as threshold)
                                            )
                                    """.format(TABLE_NAME=predictionTablePath, 
                                                PRED_LABEL=prediction_label,
                                            MODEL_NAME= modelNamePath, COL_NAMES=columnNames, 
                                            TEST_TABLE=  BQTestTablePath, THRESHOLD=threshold)

                queryJob = client.query(QUERY, job_config=jobConfig)  # API request
                helper.jobStatus([queryJob], 5)
                print('Saving model predictions into {PREDICTION_TABLE} with threshold {THRESHOLD}'.
                        format(PREDICTION_TABLE=predictionTablePath, THRESHOLD=threshold))
        
        

        if type(modelName) == list:                  
            for model in modelName:
                
                    temp = model.split('.')
                    if len(temp) == 1:
                        model = self.projectName + "." + self.datasetName + "." + model
                    
                    parts = predictionTable.split('.')
                    predictionTableName = parts[-1]
                    
                    BQTestTablePath = predictionTable
                    modelNamePath = model
                    predictionTablePath = parts[0] + '.' + parts[1] + ".p_" + predictionTableName + "_" +  model.split('.')[-1]
                    predictAssit(predictionTablePath,modelNamePath,BQTestTablePath)
        else :        
                    temp = modelName.split('.')
                    if len(temp) == 1:
                            modelName = self.projectName + "." + self.datasetName + "." + modelName
                            
                    BQTestTablePath = predictionTable
                    modelNamePath = path + "." + modelName.split('.')[-1]
                    predictionTablePath = parts[0] + '.' + parts[1] + ".p_" + predictionTableName + "_" +  model.split('.')[-1]
                    predictAssit(predictionTablePath,modelNamePath,BQTestTablePath)

               
            
    def listAllModels(self,modelName=None):
        
        self.evaluator = Evaluator(self.projectName,self.datasetName,self.keepAllTrainedModels)
        
        if modelName == None:
            modelName = self.modelName
        self.allModelsList = self.evaluator.returnModelList(modelName)

        return self.allModelsList

                

